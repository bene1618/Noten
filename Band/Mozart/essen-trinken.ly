%#(set-global-staff-size 18)

\version "2.19.65"

\header {
  title = "Essen, trinken"
  composer = "W. A. Mozart"
  tagline = ""
}

\paper {
  top-margin = 18
  left-margin = 16 
  right-margin = 16
  bottom-margin = 8
}

global = { \key g \major \time 4/4 \tempo "Allegro" }

main = \relative c'' {
  \clef "G"

  g2 b | d2. c4 | b d g, e | d2 fis |
  g4 e' d c | b c b a | g g'2 fis4 | g2 cis,\fermata |
  d2 fis | g4 b,8( c) d4 g, | fis2 d4 a' | g2 r4 e' |
  b2 a4 a | g2 r2 | \break

  <>^\markup {\bold {2.}} R1 | d2 fis | g4 b d g | fis a, a2 |
  g4 c b a | g g' g fis | g e d c | b2 g'\fermata |
  fis4 r r2 | g,2 b | d4 c c c | b2 r4 g' |
  g2( fis8 e) d( c) | b2 r | \break

  <>^\markup {\bold {3.}} R1 | R | g2 b | d2. c4 |
  b g' g fis | g e d c | b c b a8( d,) | e2. a4\fermata |
  d,4 r r2 | R1 | d2 fis | g2. c,4 |
  d2. fis4 | g2 r2 \bar "|."
}

words = \lyricmode {
  Es -- sen, trin -- ken, das er -- hält den Leib,
  s'ist doch mein lieb -- ster Zeit -- ver -- treib,
  das Es -- sen und Trin -- ken. 
  Labt mich Speis' und Trank nicht mehr, dann a -- de.
  Dann Welt, gu -- te Nacht.

  So ein Brät -- chen, ein Pas -- tet -- chen, ach,
  wenn die mei -- nem Gau -- men win -- ken, 
  mei -- nem Gau -- men win -- ken, dann.
  Ja, dann ist mein Tag voll -- bracht,
  mein Tag voll -- bracht.

  Ach, und wenn im lie -- ben vol -- len Gläs -- chen
  Gram und Sor -- gen nie -- der sin -- ken, dann.
  Al -- ler Welt dann gu -- te Nacht.
}

\score { 
  \new StaffGroup <<
    \new Staff = "main" <<
      \new Voice = "main" { \global\main }
    >>
    \new Lyrics \lyricsto "main" { \words }
  >>

  \layout {
    indent = 2\cm
  }
%  \midi { }
}
