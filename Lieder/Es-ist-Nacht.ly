\version "2.18.2"

\paper {
  top-margin = 20
  left-margin = 20
  right-margin = 15
  bottom-margin = 20
%  system-count = #6
  markup-system-spacing.basic-distance = 30
  indent = 0
  myStaffSize = #20
  #(define fonts
     (make-pango-font-tree
        "Linux Libertine" 
        "Linux Biolinum"
        "Courier Sans"
        (/ myStaffSize 20)))
}

\header {
  title = \markup {\fontsize #2 \medium "Es ist Nacht"}
  composer = "Benedikt Hunger"
  poet = "Christian Morgenstern"
  % Voreingestellte LilyPond-Tagline entfernen
  tagline = ##f
  subtitle = " "
}

global = {
  \key g \minor
  \time 4/4
  \tempo "Andante"
}

mezzoSopranoVoice = \relative c' {
  \global
  \dynamicUp
  % Die Noten folgen hier.
  R1*4 |
  r4 d2 f8( es) | d2 r4 d8 d |
  \time 3/4 g4 g c, | \time 4/4 d f2-> f8( es) |
  d4 f2-> g8( es) | \time 3/4 d4 bes c | \time 4/4 d r r2 | R1*3 |
  
  \time 2/4 bes'8 f g4 | d8 f g4 | f8 d bes4 | R2 |
  \time 3/4 d8 c bes4 bes'8 a | g4 f e | d2 r4 |
  
  d2 d4 | d d2 | d4 d d | d2 r4 | d2 d4 | d2. | d2^\markup {\italic rit.} d4 | d d d |
  \time 4/4 \key g \major \tempo "Meno mosso" g2 r | R1*3 \bar "|."
}

verse = \lyricmode {
  % Liedtext folgt hier.
  Es ist Nacht, und mein Herz kommt zu dir, hält's nicht aus, hält's nicht aus mehr bei mir.
  Legt sich dir auf die Brust wie ein Stein, sinkt hi -- nein, zu dem dei -- nen hi -- nein.
  Dort erst, dort erst kommt es zur Ruh, liegt am Grund sei -- nes e -- wi -- gen Du.
}

right = \relative c'' {
  \global
  r8 <g bes,>( <f a,> c <d g,>4 <a f'>) | r8 <g' bes,>( <f a,> c <d g,>4 <d c>) |
  r8 <g bes,>( <f a,> c <d g,>) <d bes'>( <c g'> es) | 
  << 
    { \voiceOne d8( g) c,( es) bes( d) \clef bass fis,( a} 
    \new Voice { \voiceTwo <g>4 <g> <g> \clef bass <d> }
  >>
  \oneVoice
  <bes g'>8) \clef violin
  <g'' bes,>( <f a,> c <d g,>4 <a f'>)
  r8 <g' bes,>( <f a,> c <d g,>4) 
  {\slurUp\stemUp bes'8( a | \time 3/4 g f es4)}
  <<
    { s4 | \time 4/4 \stemUp f8( bes d,4) f8( a d,4) | f8( bes d,4) f8( a d,4) | \time 3/4 d8( g d bes es c | 
      \time 4/4 d) }
    \\
    { \stemUp f8( es \stemDown | \time 4/4 d4) bes d a | d4 bes d a | \time 3/4 bes g g | \time 4/4 bes8 }
  >>
  <g' bes,>8( <f a,> c <d g,>4 <a f'>) | r8 <g' bes,>( <f a,> c <d g,>4 <d c>) |
  r8 <g bes,>( <f a,> c <d g,>) <d bes'>( <c g'> a') | 
  <<
    { f8( g) es( f) d( f) d( c | \time 2/4 bes4)  \clef violin g'8( es | bes'4) g | bes g8( es | f4) f }
    \\
    { bes,4 bes bes a | \time 2/4 f \clef violin bes | d bes8( es | f4) bes, | d es }
  >>
  \time 3/4 <f bes,>8( d <bes' f>4 <f d>) | <g c,>8( e <c' g>4 <g e>) | <d' fis,>4 fis,8( a d4\!) |
  <f! d> <d bes>2 | <fis d>4 <d b>2 | <f d>4 <d bes>2 | <fis d>4 <d a>2 |
  <f! d>4 <d bes>2 | <fis d>4 <d b>2 | <f d>4^\markup {\italic rit.} <d bes>2 | <fis d>4 <d a>2 |
  \time 4/4 \key g \major r8 <g b,>( <fis a,> c <d g,>4 <fis a,>) | r8 <g b,>( <fis a,> c <d g,>4 <d c>) |
   r8 <g b,>( <fis a,> c <d g,>4) <e g,>8( c | <b g> d <b fis> a <g d>4) r \bar "|."
}

left = \relative c' {
  \global
  g,8-\markup { \italic {sempre legato} } d' c es bes d c a | g d' c es bes c a fis | 
  g d' c es bes g' es a bes bes,es c d d, c' fis, |
  g d' c es bes d c a | g d' c es bes d 
  <<
    { \change Staff="right" \slurDown\stemDown d'4( | \time 3/4 c) g8( bes \stemDown c4) }
    \\
    { g8 f | \time 3/4 es d c g a f' }
  >>
  \time 4/4 bes bes, d f a a, d f | bes bes, d f a a, d f |
  \time 3/4 g d bes d c a | \time 4/4 g d' c es bes d c a | g d' c es bes c a fis |
  g d' c es bes g' es f | d bes g'4 f f, |
  
  \time 2/4 d'8 bes es g | f d es bes | d bes es g | bes f bes, es |
  \time 3/4 d bes d f bes f | e c e g c g | d fis a d fis4 |
  
  bes,8 d f2 | b,8 d fis2 | bes,!8 d f!2 | a,8 d fis2 |
  bes,8 d f!2 | b,8 d fis2 | bes,!8 d f!2 | a,8 d fis2 |
  
  \time 4/4 \key g \major g,8 d' c d b d a d | g, d' c d b c a fis | g d' c d b d c e | d d, d' c b4 r \bar "|."
}

%%% This function was provided by Graham Percival.
#(define (make-dynamic-extra dynamic string)
     (make-music
       'AbsoluteDynamicEvent
       'tweaks
         ;; calculate centering for text
         (list (cons (quote X-offset)
           (+ -0.5 (* -0.5 (string-length dynamic)))))
       'text
         (markup #:whiteout
           #:line (
               dynamic
               #:hspace -0.3
               #:normal-text #:italic string))
      ))

ppUnaCorda = #(make-dynamic-extra "pp" "una corda")
mpTreCorde = #(make-dynamic-extra "mp" "tre corde")

dynamics = {
  s1\ppUnaCorda s s s s s s2. s1 s s2.\< s1\mpTreCorde s s s\< s2\mf s s s s2. s s4 s2\> s2.\ppUnaCorda s s s s s s s s\pp
}

mezzoSopranoVoicePart = \new Staff \with {
  midiInstrument = "choir aahs"
  fontSize = #-3
  \override StaffSymbol.staff-space = #(magstep -3)
} { \mezzoSopranoVoice }
\addlyrics { \verse }

pianoPart = \new PianoStaff <<
  \new Staff = "right" \with {
    midiInstrument = "acoustic grand"
  } \right
  \new Dynamics \dynamics
  \new Staff = "left" \with {
    midiInstrument = "acoustic grand"
  } { \clef bass \left }
>>

\score {
  <<
    \mezzoSopranoVoicePart
    \pianoPart
  >>
  \layout { }
%  \midi {
%    \tempo 4=100
%  }
}
